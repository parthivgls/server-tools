# Copyright 2017 LasLabs Inc.
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).

{
    'name': 'Module Auto Update',
    'summary': 'Automatically update Odoo modules',
    'version': '1.0.2.0.4',
    'category': 'Extra Tools',
    'website': 'https://gitlab.com/flectra-community/server-tools',
    'author': 'LasLabs, '
              'Juan José Scarafía, '
              'Tecnativa, '
              'ACSONE SA/NV, '
              'Flectra Community, Odoo Community Association (OCA)',
    'license': 'LGPL-3',
    'application': False,
    'installable': False,
    'uninstall_hook': 'uninstall_hook',
    'depends': [
        'base',
    ],
    'data': [
        'data/cron_data_deprecated.xml',
    ],
    'development_status': 'Production/Stable',
    'maintainers': ['sbidoul'],
}
