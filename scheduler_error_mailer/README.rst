*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

======================
Scheduler Error Mailer
======================

This module adds the possibility to send an e-mail when a scheduler raises
an error.

Configuration
=============

To configure this module, you need to:

#. Go to Settings -> Technical -> Automation -> Scheduled Actions
#. Choose the scheduled Actions you want to send the error email and select the E-mail Template in the Error E-mail Template field.

Usage
=====

To use this module, you need to:

Credits
=======

Images
------

* Odoo Community Association: `Icon <https://github.com/OCA/maintainer-tools/blob/master/template/module/static/description/icon.svg>`_.

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Sébastien BEAU <sebastien.beau@akretion.com>
* David Beal <bealdavid@gmail.com>
* Alexis de Lattre <alexis.delattre@akretion.com>
* Sodexis <dev@sodexis.com>
* Achraf Mhadhbi <machraf@bloopark.de>