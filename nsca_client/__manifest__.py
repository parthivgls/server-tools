# © 2015 ABF OSIELL <http://osiell.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    "name": "NSCA Client",
    "summary": "Send passive alerts to monitor your Odoo application.",
    "version": "1.0.1.0.0",
    "category": "Tools",
    "website": "https://gitlab.com/flectra-community/server-tools",
    "author": "ABF OSIELL, Flectra Community, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "data": [
        "security/ir.model.access.csv",
        "views/nsca_menu.xml",
        "views/nsca_check.xml",
        "views/nsca_server.xml",
    ],
    "demo": [
        "demo/demo_data.xml",
    ],
}
