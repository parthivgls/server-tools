*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: https://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3

===================
Base Cron Exclusion
===================

This module extends the functionality of scheduled actions to allow you to
select the ones that should not run simultaneously.

Usage
=====

To use this module, you need to:

#. Go to *Settings > Technical > Automation > Scheduled Actions*.
#. In the form view go to the tab *Mutually Exclusive Scheduled Actions*.
#. Fill it with the actions that should be blocked while running the action
   you are editing. Note that this is mutual and the selected actions will
   block the initial action when running.

Credits
=======

Images
------

* Odoo Community Association: `Icon <https://github.com/OCA/maintainer-tools/blob/master/template/module/static/description/icon.svg>`_.

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Lois Rilo <lois.rilo@eficent.com>
* Jordi Ballester <jordi.ballester@eficent.com>
* Bhavesh Odedra <bodedra@opensourceintegrators.com>

Do not contact contributors directly about support or help with technical issues.