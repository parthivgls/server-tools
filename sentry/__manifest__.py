# Copyright 2016-2017 Versada <https://versada.eu/>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    'name': 'Sentry',
    'summary': 'Report Odoo errors to Sentry',
    'version': '1.0.1.1.0',
    'category': 'Extra Tools',
    'website': 'https://gitlab.com/flectra-community/server-tools',
    'author': 'Mohammed Barsi,'
              'Versada,'
              'Nicolas JEUDY,'
              'Flectra Community, Odoo Community Association (OCA)',
    'license': 'AGPL-3',
    'application': False,
    'installable': True,
    'external_dependencies': {
        'python': [
            'raven',
        ]
    },
    'depends': [
        'base',
    ],
}
