*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
    :target: https://www.gnu.org/licenses/agpl-3.0-standalone.html
    :alt: License: AGPL-3

=================================
Audit Log - Track user operations
=================================

This module allows the administrator to log user operations performed on data
models such as ``create``, ``read``, ``write`` and ``delete``.

Usage
=====

Go to `Settings / Technical / Audit / Rules` to subscribe rules. A rule defines
which operations to log for a given data model.

.. image:: /auditlog/static/description/rule.png

Then, check logs in the `Settings / Technical / Audit / Logs` menu. You can
group them by user sessions, date, data model or HTTP requests:

.. image:: /auditlog/static/description/logs.png

Get the details:

.. image:: /auditlog/static/description/log.png

A scheduled action exists to delete logs older than 6 months (180 days)
automatically but is not enabled by default.
To activate it and/or change the delay, go to the
`Configuration / Technical / Automation / Scheduled Actions` menu and edit the
`Auto-vacuum audit logs` entry:

.. image:: /auditlog/static/description/autovacuum.png

Known issues / Roadmap
======================

 * log only operations triggered by some users (currently it logs all users)


Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Sebastien Alix <sebastien.alix@osiell.com>
* Holger Brunn <hbrunn@therp.nl>
* Holden Rehg <holdenrehg@gmail.com>

Images
------

* Icon: built with different icons from the `Oxygen theme <https://en.wikipedia.org/wiki/Oxygen_Project>`_ (LGPL)