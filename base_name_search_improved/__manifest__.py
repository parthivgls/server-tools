# Copyright 2016 Daniel Reis
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    'name': 'Improved Name Search',
    'summary': 'Friendlier search when typing in relation fields',
    'version': '1.0.1.0.0',
    'category': 'Uncategorized',
    'website': 'https://gitlab.com/flectra-community/server-tools',
    'author': 'Daniel Reis, Flectra Community, Odoo Community Association (OCA)',
    'license': 'AGPL-3',
    'data': [
        'views/ir_model_views.xml',
    ],
    'depends': [
        'base',
    ],
    'installable': True,
}
