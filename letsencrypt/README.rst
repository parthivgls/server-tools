*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

=============================================
Request SSL certificates from letsencrypt.org
=============================================

This module was written to have your Odoo installation request SSL certificates
from https://letsencrypt.org automatically.

Installation
============

After installation, this module generates a private key for your account at
letsencrypt.org automatically in ``$data_dir/letsencrypt/account.key``. If you
want or need to use your own account key, replace the file.

For certificate requests to work, your site needs to be accessible via plain
HTTP, see below for configuration examples in case you force your clients to
the SSL version.

After installation, trigger the cronjob `Update letsencrypt certificates` and
watch your log for messages.

This addon depends on the ``openssl`` binary and the ``acme_tiny`` and ``IPy``
python modules.

For installing the OpenSSL binary you can use your distro package manager.
For Debian and Ubuntu, that would be:

    sudo apt-get install openssl

For installing the ACME-Tiny python module, use the PIP package manager:

    sudo pip install acme-tiny

For installing the IPy python module, use the PIP package manager:

    sudo pip install IPy


Configuration
=============

This addons requests a certificate for the domain named in the configuration
parameter ``web.base.url`` - if this comes back as ``localhost`` or the like,
the module doesn't request anything.

If you want your certificate to contain multiple alternative names, just add
them as configuration parameters ``letsencrypt.altname.N`` with ``N`` starting
from ``0``. The amount of domains that can be added are subject to `rate
limiting <https://community.letsencrypt.org/t/rate-limits-for-lets-encrypt/6769>`_.

Note that all those domains must be publicly reachable on port 80 via HTTP, and
they must have an entry for ``.well-known/acme-challenge`` pointing to your odoo
instance.

Usage
=====

The module sets up a cronjob that requests and renews certificates automatically.

After the first run, you'll find a file called ``domain.crt`` in
``$datadir/letsencrypt``, configure your SSL proxy to use this file as certificate.

Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Holger Brunn <hbrunn@therp.nl>
* Antonio Espinosa <antonio.espinosa@tecnativa.com>
* Dave Lasley <dave@laslabs.com>
* Ronald Portier <ronald@therp.nl>
* Ignacio Ibeas <ignacio@acysos.com>

ACME implementation
-------------------

* https://github.com/diafygi/acme-tiny/blob/master/acme_tiny.py

Icon
----

* https://helloworld.letsencrypt.org