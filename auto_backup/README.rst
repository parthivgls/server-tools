*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/license-AGPL--3-blue.png
   :target: https://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3

====================
Database Auto-Backup
====================

A tool for all your back-ups, internal and external!

Installation
============

Before installing this module, you need to execute::

    pip3 install pysftp

Configuration
=============

Go to *Settings -> Database Structure -> Automated Backup* to
create your configurations for each database that you needed
to backups.

Usage
=====

Keep your Odoo data safe with this module. Take automated back-ups,
remove them automatically and even write them to an external server
through an encrypted tunnel. You can even specify how long local backups
and external backups should be kept, automatically!

Connect with an FTP Server
--------------------------

Keep your data safe, through an SSH tunnel!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Want to go even further and write your backups to an external server?
You can with this module! Specify the credentials to the server, specify
a path and everything will be backed up automatically. This is done
through an SSH (encrypted) tunnel, thanks to pysftp, so your data is
safe!

Test connection
---------------

Checks your credentials in one click
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Want to make sure if the connection details are correct and if Odoo can
automatically write them to the remote server? Simply click on the ‘Test
SFTP Connection’ button and you will get message telling you if
everything is OK, or what is wrong!

E-mail on backup failure
------------------------

Stay informed of problems, automatically!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Do you want to know if the database backup succeeded or failed? Subscribe to
the corresponding backup setting notification type.

Run backups when you want
-------------------------

From the backups configuration list, press *More > Execute backup(s)* to
manually execute the selected processes.

Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Yenthe Van Ginneken <yenthe.vanginneken@vanroey.be>
* Alessio Gerace <alessio.gerace@agilebg.com>
* Jairo Llopis <yajo.sk8@gmail.com>
* Dave Lasley <dave@laslabs.com>
* Andrea Stirpe <a.stirpe@onestein.nl>