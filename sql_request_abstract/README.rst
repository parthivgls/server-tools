*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.png
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

=====================================
Abstract Model to manage SQL Requests
=====================================

This module provides an abstract model to manage SQL Select requests on database.
It is not usefull for itself. You can see an exemple of implementation in the
'sql_export' module. (same repository).

Implemented features
--------------------

* Add some restrictions in the sql request:
    * you can only read datas. No update, deletion or creation are possible.
    * some tables are not allowed, because they could contains clear password
      or keys. For the time being ('ir_config_parameter').

* The request can be in a 'draft' or a 'SQL Valid' status. To be valid,
  the request has to be cleaned, checked and tested. All of this operations
  can be disabled in the inherited modules.

* This module two new groups:
    * SQL Request / User : Can see all the sql requests by default and execute
      them, if they are valid.
    * SQL Request / Manager : has full access on sql requests.

Usage
=====

Inherit the model:

    from openerp import models

    class MyModel(models.model)
        _name = 'my.model'
        _inherit = ['sql.request.mixin']

        _sql_request_groups_relation = 'my_model_groups_rel'

        _sql_request_users_relation = 'my_model_users_rel'


Credits
=======

Images
------

* Odoo Community Association: `Icon <https://github.com/OCA/maintainer-tools/blob/master/template/module/static/description/icon.svg>`_.

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Florian da Costa <florian.dacosta@akretion.com>
* Sylvain LE GAL (https://twitter.com/legalsylvain)

Funders
-------

The development of this module has been financially supported by:

* Akretion (<http://www.akretion.com>)
* GRAP, Groupement Régional Alimentaire de Proximité (<http://www.grap.coop>)