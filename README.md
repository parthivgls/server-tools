# Flectra Community / Server Tools

Tools for Flectra Administrators to improve some technical features.



Available addons
----------------

addon | version | summary
--- | --- | ---
[sentry](sentry/) | 1.0.1.1.0| Report Odoo errors to Sentry
[fetchmail_notify_error_to_sender](fetchmail_notify_error_to_sender/) | 1.0.1.0.0| If fetching mails gives error, send an email to sender
[datetime_formatter](datetime_formatter/) | 1.0.1.0.0| Helper functions to give correct format to date[time] fields
[configuration_helper](configuration_helper/) | 1.0.1.0.0| Configuration Helper
[scheduler_error_mailer](scheduler_error_mailer/) | 1.0.1.0.0| Scheduler Error Mailer
[record_archiver](record_archiver/) | 1.0.1.0.0| Records Archiver
[base_remote](base_remote/) | 1.0.1.0.2| Remote Base
[auditlog](auditlog/) | 1.0.1.0.0| Audit Log
[sql_request_abstract](sql_request_abstract/) | 1.0.1.0.1| Abstract Model to manage SQL Requests
[letsencrypt](letsencrypt/) | 1.0.1.0.0| Request SSL certificates from letsencrypt.org
[base_exception](base_exception/) | 1.0.1.1.0|     This module provide an abstract model to manage customizable    exceptions to be applied on different models (sale order, invoice, ...)
[dbfilter_from_header](dbfilter_from_header/) | 1.0.1.0.0| Filter databases with HTTP headers
[test_configuration_helper](test_configuration_helper/) | 1.0.1.0.0| Configuration Helper - Tests
[module_auto_update](module_auto_update/) | 1.0.2.0.4| Automatically update Odoo modules
[base_search_fuzzy](base_search_fuzzy/) | 1.0.1.0.0| Fuzzy search with the PostgreSQL trigram extension
[base_name_search_improved](base_name_search_improved/) | 1.0.1.0.0| Friendlier search when typing in relation fields
[database_cleanup](database_cleanup/) | 1.0.1.0.0| Database cleanup
[onchange_helper](onchange_helper/) | 1.0.1.0.0| Technical module that ease execution of onchange in Python code
[html_image_url_extractor](html_image_url_extractor/) | 1.0.1.0.0| Extract images found in any HTML field
[nsca_client](nsca_client/) | 1.0.1.0.0| Send passive alerts to monitor your Odoo application.
[fetchmail_incoming_log](fetchmail_incoming_log/) | 1.0.1.0.0| Log all messages received, before they start to be processed.
[base_fontawesome](base_fontawesome/) | 1.0.5.3.1| Up to date Fontawesome resources.
[base_technical_user](base_technical_user/) | 1.0.1.0.0|         Add a technical user parameter on the company 
[html_text](html_text/) | 1.0.1.0.2| Generate excerpts from any HTML field
[auto_backup](auto_backup/) | 1.0.1.0.0| Backups database
[base_cron_exclusion](base_cron_exclusion/) | 1.0.1.0.0| Allow you to select scheduled actions that should not run simultaneously.


