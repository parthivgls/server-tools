*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/license-AGPL--3-blue.png
   :target: https://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3

==========================
Image URLs from HTML field
==========================

This module includes a method that extracts image URLs from any chunk of HTML,
in appearing order.

Usage
=====

This module just adds a technical utility, but nothing for the end user.

If you are a developer and need this utility for your module, see these
examples and read the docs inside the code.

Python example::

    @api.multi
    def some_method(self):
        # Get images from an HTML field
        imgs = self.env["ir.fields.converter"].imgs_from_html(self.html_field)
        for url in imgs:
            # Do stuff with those URLs
            pass

QWeb example::

    <!-- Extract first image from a blog post -->
    <t t-foreach="env['ir.fields.converter']
                  .imgs_from_html(blog_post.content, 1)"
       t-as="url">
        <img t-att-href="url"/>
    </t>

Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Jairo Llopis <yajo.sk8@gmail.com>
* Vicent Cubells <vicent.cubells@tecnativa.com>
* Dennis Sluijk <d.sluijk@onestein.nl>

Do not contact contributors directly about support or help with technical issues.