*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/license-AGPL--3-blue.png
   :target: https://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3

===================
Base Technical User
===================

This module extends the functionality of company management.
It allows you to bind a technical user on the company in order to use it in
batch processes.

The technical user must
- be inactive to avoid login
- be in the required groups depending of what you need to do

Usage
=====

If you install the module, you will find a tab on the company form allowing
to define the technical user.

.. image:: https://odoo-community.org/website/image/ir.attachment/5784_f2813bd/datas
   :alt: Try me on Runbot
   :target: https://runbot.odoo-community.org/runbot/149/11.0

Credits
=======

Images
------

* Odoo Community Association: `Icon <https://github.com/OCA/maintainer-tools/blob/master/template/module/static/description/icon.svg>`_.

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Cédric Pigeon <cedric.pigeon@acsone.eu>