*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/license-AGPL--3-blue.png
   :target: https://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3

====================
Text from HTML field
====================

This module provides some technical features that allow to extract text from
any chunk of HTML, without HTML tags or attributes. You can chose either:

* To truncate the result by amount of words or characters.
* To append an ellipsis (or any character(s)) at the end of the result.

It can be used to easily generate excerpts.

Usage
=====

This module just adds a technical utility, but nothing for the end user.

If you are a developer and need this utility for your module, see these
examples and read the docs inside the code.

Python example::

    @api.multi
    def some_method(self):
        # Get truncated text from an HTML field. It will 40 words and 100
        # characters at most, and will have "..." appended at the end if it
        # gets truncated.
        truncated_text = self.env["ir.fields.converter"].text_from_html(
            self.html_field, 40, 100, "...")

QWeb example::

    <t t-esc="env['ir.fields.converter'].text_from_html(doc.html_field)"/>

Credits
=======

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Jairo Llopis <yajo.sk8@gmail.com>
* Vicent Cubells <vicent.cubells@tecnativa.com>
* Dennis Sluijk <d.sluijk@onestein.nl>

Do not contact contributors directly about support or help with technical issues.