*This module was migrated automatically from OCA repository* 
*to flectra community repository. We do not guarantee the correctness of all information.*
*Please check https://gitlab.com/flectra-community/oca2fc/blob/master/README.md*
*fur further informations about automatic migration.*

.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

============================
Configuration Helper - TESTS
============================

*This module is only intended to test configuration_helper module.*

Contributors
------------

* Flectra Community <info@flectra-community.org>
* Yannick Vaucher <yannick.vaucher@camptocamp.com>